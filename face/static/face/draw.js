$(function(){
  // var video = document.getElementById('v1');
  var video = $("#v1");
  var video_width = video[0].offsetWidth;
  var video_height = video[0].offsetHeight;
  console.log('video size', video_width, video_height);

  var canvas = this.__canvases = new fabric.Canvas('c1');
  canvas.setHeight(video_height);
  canvas.setWidth(video_width);
  console.log('convas size is set with width=', canvas.width);

  var isPlay = false;
  var skip_val = 5;
  var isUpdated = false;
  var latest_rect = 0;

  canvas.on(
  'mouse:down', 
  function(options) {
      isUpdated = true;

      if(isPlay){
        video[0].pause();
        $('#button_play').text("Play");
        isPlay = false;
      }

      if(options.target){
          return;
      }

      var pointer = canvas.getPointer(event.e);
      var size = 50;
      var x = pointer.x - size/2;
      var y = pointer.y - size/2;
      canvas.add(new fabric.Rect({
        left:x,top:y,width:size,
        height:size,
        hasRotatingPoint:false,
        fill:'rgba(125,125,40,0.4)',
        stroke:'red',
        strokeWidth:1, 
        id:latest_rect}))
      latest_rect += 1;
      console.log('add with id ', canvas.getObjects()[canvas.getObjects().length-1].id);
  });

  fabric.util.addListener(canvas.upperCanvasEl, 'dblclick', function(options){
      if(options.target){
        tar_obj = canvas.getActiveObject();
        canvas.remove(tar_obj);
        console.log('deleted object with id', tar_obj.id);
        return;
      }
    });

  // the video player control
  video.on('timeupdate', function(){
    $('.curret').text(video[0].currentTime);
    var currentPos = video[0].currentTime;
    var maxDuration = video[0].duration;
    var percentage = 100*currentPos / maxDuration;
    $('.timeBar').css('width', percentage+'%');
  });

  $("#button_play").click(function(){
    var button = $('#button_play');
    if(isPlay){
      video[0].pause();
      button.text("Play");
    }

    else{
      if(isUpdated){
        isUpdated = false;

        objects = canvas.getObjects();
        bbxes = []
        for(var i=0; i<objects.length; i++){
          acoord = objects[i].aCoords;

          bbxes.push({
            'left':acoord.tl.x,
            'top':acoord.tl.y,
            'width':acoord.br.x - acoord.tl.x,
            'height':acoord.br.y - acoord.tl.y,
            'id':objects[i].id
          });

          bbx = bbxes[bbxes.length-1]
          console.log('x1=', bbx.left, 'y1=', bbx.top, 
            'x2=', bbx.left+bbx.width, 'y2=', bbx.top+bbx.height);
        }

        send_data = {
          'time':video[0].currentTime,
          'bbx':bbxes,
          'can_width':canvas.width
        }
        $.ajax({
          url: '/face/update_bbx/',
          data: {
            'bbx_list':JSON.stringify(send_data)
          },
          dataType: 'json',
          success: function(data){
            console.log('current time', video[0].currentTime);
          }
        });
      }

      video[0].play();
      button.text("Pause");
    }

    isPlay = !isPlay;
  });

  $("#button_back").click(function(){
    console.log('before current', video[0].currentTime);
    console.log('duration', video[0].duration);
    video[0].currentTime = Math.max(0, video[0].currentTime-skip_val);
    console.log('after current', video[0].currentTime);
    // video[0].playbackRate = Math.max(1.0, video[0].playbackRate-1.0);
    return false;
  });

  $("#button_forward").click(function(){
    console.log('before current', video[0].currentTime);
    console.log('duration', video[0].duration);
    video[0].currentTime = Math.min(video[0].duration-1, video[0].currentTime+skip_val);
    console.log('after current', video[0].currentTime);
    // video[0].playbackRate = Math.min(8.0, video[0].playbackRate+1.0);
    return false;
  });

  // progress bar
  vid_progress = $("#vidprogress");
  vid_progress.progressbar({
     value: 0,
     max: 100
  });
  video.on('timeupdate', function(){
  // Calculate the precentage value
  var value = (100 / video[0].duration) * video[0].currentTime;
  // Update the value
  vid_progress.progressbar("value", value);
});

  setInterval(
     function(){   
        if (!isPlay){
          return
        }
        objects = canvas.getObjects();
        if (objects.length == 0){
          return;
        }

        bbxes = []
        for(var i=0; i<objects.length; i++){
          acoord = objects[i].aCoords;

          bbxes.push({
            'left':acoord.tl.x,
            'top':acoord.tl.y,
            'width':acoord.br.x - acoord.tl.x,
            'height':acoord.br.y - acoord.tl.y,
            'id':objects[i].id
          });

          bbx = bbxes[bbxes.length-1]
          console.log('x1=', bbx.left, 'y1=', bbx.top, 
            'x2=', bbx.left+bbx.width, 'y2=', bbx.top+bbx.height);
        }

        send_data = {
          'time':video[0].currentTime,
          'bbx':bbxes,
          'can_width':canvas.width
        }
        $.ajax({
          url: '/face/update_bbx/',
          data: {
            'bbx_list':JSON.stringify(send_data)
          },
          dataType: 'json',
          success: function(data){
            console.log('current time', video[0].currentTime);
          }
        });
     },
     500  /* 1000 ms = 1 sec */
  );

});
