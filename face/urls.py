from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.get_path, name='get_path'),
    url(r'^annotation/$', views.annotation, name='annotation'),
    url(r'^update_bbx/$', views.update_frm, name='update_frm'),
    url(r'^finish/$', views.submit_video, name='submit_video'),
]