from collections import namedtuple
import cv, cv2, os, numpy as np
from scipy.misc import imresize


Bbx = namedtuple('Bbx', 'x, y, w, h, time')

class Track:
	def __init__(self):
		self.start, self.end = 0, 0
		self.track_list = []

	def append(self, bbx, time):
		self.track_list.append(Bbx(bbx['left'], bbx['top'], bbx['width'], bbx['height'], time))

	def query(self, time):
		''' query the corresponding bbx given the time
		'''
		if time<self.start or time>=self.end:
			return None

		def interpolate(bbx_1, bbx_2, time):
			# return Bbx(bbx_1.x, bbx_1.y, bbx_1.w, bbx_1.h, time)
			ratio = (time - bbx_1.time) / (bbx_2.time - bbx_1.time)
			y = bbx_1.y + ratio*(bbx_2.y - bbx_1.y)
			x = bbx_1.x + ratio*(bbx_2.x - bbx_1.x)
			h = bbx_1.h + ratio*(bbx_2.h - bbx_1.h)
			w = bbx_1.w + ratio*(bbx_2.w - bbx_1.w)
			tar_bbx = Bbx(x, y, w, h, time)
			return tar_bbx

		for idx in range(1, len(self.track_list)):
			if time < self.track_list[idx].time:
				break

		return interpolate(self.track_list[idx-1], self.track_list[idx], time)

	def sort(self):
		self.track_list = sorted(self.track_list, key=lambda d: d.time)
		self.start = self.track_list[0].time
		self.end =self.track_list[-1].time

class TrackSet:
	def __init__(self):
		self.tracks = {}

	def clear(self):
		self.tracks = {}

	def append(self, bbxes, time):
		for bbx in bbxes:
			bbx_id = bbx['id']
			if not bbx_id in self.tracks.keys():
				self.tracks[bbx_id] = Track()

			self.tracks[bbx_id].append(bbx, time)

	def pixelateFrm(self, frm, time, size_ratio):
		def pixelate(src_region, factor=10):
			return imresize(imresize(src_region, (src_region.shape[0]/factor, src_region.shape[1]/factor)), src_region.shape)

		bbxes = [track.query(time) for track_id, track in self.tracks.iteritems()]
		bbxes = [bbx for bbx in bbxes if bbx != None]
		frm_width, frm_height = frm.shape[1], frm.shape[0]

		tar = frm.copy()
		for bbx in bbxes:
			pt1 = (int(bbx.x*size_ratio), int(bbx.y*size_ratio))
			pt2 = (int((bbx.x+bbx.w)*size_ratio), int((bbx.y+bbx.h)*size_ratio))

			clamp_x = lambda x : min(max(x, 0), frm_width-1)
			clamp_y = lambda y : min(max(y, 0), frm_height-1)

			pt1 = (clamp_x(pt1[0]), clamp_y(pt1[1]))
			pt2 = (clamp_x(pt2[0]), clamp_y(pt2[1]))

			tar[pt1[1]:pt2[1], pt1[0]:pt2[0]] = \
				pixelate(tar[pt1[1]:pt2[1], pt1[0]:pt2[0]])
			# cv2.rectangle(tar, pt1, pt2, (255,0,0), 2)

		return tar

	def saveVideo(self, vid_path, can_width):
		for key in self.tracks.keys():
			self.tracks[key].sort()

		cap = cv2.VideoCapture(vid_path)

		frm_rate = cap.get(cv.CV_CAP_PROP_FPS)
		frm_size = (int(cap.get(cv.CV_CAP_PROP_FRAME_WIDTH)), int(cap.get(cv.CV_CAP_PROP_FRAME_HEIGHT)))
		size_ratio = float(frm_size[0])/float(can_width)

		fourcc = cv2.cv.CV_FOURCC('X','V','I','D')
		print 'video loaded with fps = {}, frm_size = {}'.format(frm_rate, frm_size)

		masked_vid_path = vid_path[0:vid_path.rfind('.')]+'_blurred.avi'
		if os.path.exists(masked_vid_path):
			os.remove(masked_vid_path)

		out_vid = cv2.VideoWriter(masked_vid_path, fourcc, frm_rate, frm_size)
		
		frmIdx = 0
		while(cap.isOpened()):
			ret, frm = cap.read()
			if ret == False:
				break

			out_vid.write(self.pixelateFrm(frm, frmIdx/frm_rate, size_ratio))
			frmIdx += 1

			if frmIdx%500 == 0:
				print frmIdx, 'frames has been processed'
			if frmIdx%3000 == 0:
				break

		print 'total frame size = %d'%(frmIdx)
		cap.release()
		out_vid.release()
