# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.template import loader
from django.http import HttpResponse, JsonResponse, HttpResponseRedirect
import json, sys, os

from .forms import PathForm
from .track import *
from django.conf import settings

# Create your views here.
media_root = settings.MEDIA_ROOT
tracks = TrackSet()
curr_vid = ''
can_width = 0

def get_path(request):
    global curr_vid, can_width, tracks
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = PathForm(request.POST)
        # check whether it's valid:
        if form.is_valid():
            # process the data in form.cleaned_data as required
            # ...
            # redirect to a new URL:
            curr_vid = form.cleaned_data['path']
            vid_path = os.path.join(media_root, curr_vid)
            if not os.path.exists(vid_path):
                return HttpResponse('%s does not exist'%vid_path)


            return HttpResponseRedirect('/face/annotation/')

    form = PathForm()
    return render(request, 'face/index.html', {'form': form})

def annotation(request):
    global curr_vid, can_width, tracks
    tracks.clear()
    print 'into annotation', request
    template = loader.get_template('face/draw.html')
    context = {'video_path':'http://127.0.0.1/%s'%curr_vid}
    return HttpResponse(template.render(context, request))

def update_frm(request):
    global curr_vid, can_width, tracks
    jsonstring = request.GET.get('bbx_list', None)
    data = json.loads(jsonstring)
    time = data['time']
    bbx = data['bbx']
    can_width = data['can_width']
    tracks.append(bbx, time)
    return JsonResponse({'bbx':len(bbx)})

def submit_video(request):
    global curr_vid, can_width, tracks
    vid_path = os.path.join(media_root, curr_vid)
    tracks.saveVideo(vid_path, can_width)
    return HttpResponse("the pixelate video is processed")